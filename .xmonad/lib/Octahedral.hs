module Octahedral where

import XMonad
import XMonad.StackSet as WindowStack

type OctahedralPermutation = [Int]

mult :: [Int] -> [Int] -> [Int]
mult g [] = []
mult g (i:is) = g !! i : mult g is 

inv :: [Int] -> [Int]
inv g = map (\i -> inv_func g i 0) identity

inv_func :: [Int] -> Int -> Int -> Int
inv_func [] _ _ = -1
inv_func (x:xs) value index = if x==value
                              then index
                              else inv_func xs value (index+1)

octahedral_group :: [OctahedralPermutation]
octahedral_group = [
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23],
    [1, 2, 3, 0, 15, 12, 13, 14, 7, 4, 5, 6, 17, 18, 19, 16, 9, 10, 11, 8, 23, 20, 21, 22],
    [2, 3, 0, 1, 16, 17, 18, 19, 14, 15, 12, 13, 10, 11, 8, 9, 4, 5, 6, 7, 22, 23, 20, 21],
    [3, 0, 1, 2, 9, 10, 11, 8, 19, 16, 17, 18, 5, 6, 7, 4, 15, 12, 13, 14, 21, 22, 23, 20],
    [4, 5, 6, 7, 8, 9, 10, 11, 0, 1, 2, 3, 20, 21, 22, 23, 12, 13, 14, 15, 16, 17, 18, 19],
    [5, 6, 7, 4, 23, 20, 21, 22, 11, 8, 9, 10, 13, 14, 15, 12, 1, 2, 3, 0, 19, 16, 17, 18],
    [6, 7, 4, 5, 12, 13, 14, 15, 22, 23, 20, 21, 2, 3, 0, 1, 8, 9, 10, 11, 18, 19, 16, 17],
    [7, 4, 5, 6, 1, 2, 3, 0, 15, 12, 13, 14, 9, 10, 11, 8, 23, 20, 21, 22, 17, 18, 19, 16],
    [8, 9, 10, 11, 0, 1, 2, 3, 4, 5, 6, 7, 16, 17, 18, 19, 20, 21, 22, 23, 12, 13, 14, 15],
    [9, 10, 11, 8, 19, 16, 17, 18, 3, 0, 1, 2, 21, 22, 23, 20, 5, 6, 7, 4, 15, 12, 13, 14],
    [10, 11, 8, 9, 20, 21, 22, 23, 18, 19, 16, 17, 6, 7, 4, 5, 0, 1, 2, 3, 14, 15, 12, 13],
    [11, 8, 9, 10, 5, 6, 7, 4, 23, 20, 21, 22, 1, 2, 3, 0, 19, 16, 17, 18, 13, 14, 15, 12],
    [12, 13, 14, 15, 22, 23, 20, 21, 6, 7, 4, 5, 18, 19, 16, 17, 2, 3, 0, 1, 8, 9, 10, 11],
    [13, 14, 15, 12, 17, 18, 19, 16, 21, 22, 23, 20, 3, 0, 1, 2, 7, 4, 5, 6, 11, 8, 9, 10],
    [14, 15, 12, 13, 2, 3, 0, 1, 16, 17, 18, 19, 4, 5, 6, 7, 22, 23, 20, 21, 10, 11, 8, 9],
    [15, 12, 13, 14, 7, 4, 5, 6, 1, 2, 3, 0, 23, 20, 21, 22, 17, 18, 19, 16, 9, 10, 11, 8],
    [16, 17, 18, 19, 14, 15, 12, 13, 2, 3, 0, 1, 22, 23, 20, 21, 10, 11, 8, 9, 4, 5, 6, 7],
    [17, 18, 19, 16, 21, 22, 23, 20, 13, 14, 15, 12, 11, 8, 9, 10, 3, 0, 1, 2, 7, 4, 5, 6],
    [18, 19, 16, 17, 10, 11, 8, 9, 20, 21, 22, 23, 0, 1, 2, 3, 14, 15, 12, 13, 6, 7, 4, 5],
    [19, 16, 17, 18, 3, 0, 1, 2, 9, 10, 11, 8, 15, 12, 13, 14, 21, 22, 23, 20, 5, 6, 7, 4],
    [20, 21, 22, 23, 18, 19, 16, 17, 10, 11, 8, 9, 14, 15, 12, 13, 6, 7, 4, 5, 0, 1, 2, 3],
    [21, 22, 23, 20, 13, 14, 15, 12, 17, 18, 19, 16, 7, 4, 5, 6, 11, 8, 9, 10, 3, 0, 1, 2],
    [22, 23, 20, 21, 6, 7, 4, 5, 12, 13, 14, 15, 8, 9, 10, 11, 18, 19, 16, 17, 2, 3, 0, 1],
    [23, 20, 21, 22, 11, 8, 9, 10, 5, 6, 7, 4, 19, 16, 17, 18, 13, 14, 15, 12, 1, 2, 3, 0]
    ]
 
identity :: OctahedralPermutation
identity = [0, 1, 2, 3, 4,5,6,7,8,9,10,11,12,13,14,15,
            16,17,18,19,20,21,22,23]
            
-- Corresponds to moving the head downwards, as if to say "yes" but only the first part of the move
yes :: OctahedralPermutation
yes = [15, 12, 13, 14, 7, 4, 5, 6, 1, 2, 3, 0, 23, 20, 21, 22, 17, 18, 19, 16, 9, 10, 11, 8]

-- Corresponds to moving the head right, as if to say "no" but only the first part of the move
no :: OctahedralPermutation
no = [5, 6, 7, 4, 23, 20, 21, 22, 11, 8, 9, 10, 13, 14, 15, 12, 1, 2, 3, 0, 19, 16, 17, 18]

-- Corresponds to tilting the head to the right, bringing the right ear to the right shoulder
-- Called the "maybe rotation" by analogy with the previous movements.
maybe :: OctahedralPermutation
maybe = [1, 2, 3, 0, 15, 12, 13, 14, 7, 4, 5, 6, 17, 18, 19, 16, 9, 10, 11, 8, 23, 20, 21, 22]


scanChar :: Char -> Maybe Int
scanChar c | '0' <= c && c <= '9' = Just $ fromEnum c - fromEnum '0'
           | otherwise = Nothing


scanString :: String -> Int
scanString = go 0
    where go a [] = a
          go a (x:xs) = case (scanChar x) of
                            Nothing -> default_return_value
                            Just value -> go (10*a+value) xs
              where default_return_value = 1


-- Go to workspace following movement `rotation`
-- This function assumes that you have 24 workspaces, called "1", "2", ... "24"
-- To use other names check the `cubic_switch_custom` function.
-- Pressing m on face g, we go to face $gmg^{1}g$, where $gmg^{-1}$ is movement m expressed from g.
cubic_switch :: OctahedralPermutation -> WindowSet -> WindowSet
cubic_switch = cubic_switch_custom label2index_1_to_24 index2label_1_to_24
    where label2index_1_to_24 label = scanString label - 1
          index2label_1_to_24 workspace_idx = show $ workspace_idx + 1


-- Go to workspace following movement `rotation`
-- We allow 2 functions `label2index` and `index2label` in order to convert to and from
-- the xmonad workspace label (a String that can be whatever you want) to the standard indexes 
-- that goes from 0 to 23.
-- Pressing m on face g, we go to face $gmg^{1}g$, where $gmg^{-1}$ is movement m expressed from g.
cubic_switch_custom :: (String -> Int) -> (Int -> String) -> OctahedralPermutation -> WindowSet -> WindowSet
cubic_switch_custom label2index index2label rotation window_set = 
	WindowStack.view (index2label new_workspace_idx) window_set
    where current_workspace_element = octahedral_group !! (label2index $ WindowStack.currentTag window_set)
          local_rotation = mult current_workspace_element $ mult rotation $ inv current_workspace_element
          new_workspace_idx = (mult local_rotation $ current_workspace_element) !! 0


-- Shift focused window to workspace and view the workspace
-- Provide functions to map to and from the standard workspace indices 0, .., 23
cubic_shift_custom :: (String -> Int) -> (Int -> String) -> OctahedralPermutation -> WindowSet -> WindowSet
cubic_shift_custom label2index index2label rotation window_set = 
	WindowStack.view new_workspace_label $ WindowStack.shift new_workspace_label  window_set
    where current_workspace_element = octahedral_group !! (label2index $ WindowStack.currentTag window_set)
          local_rotation = mult current_workspace_element $ mult rotation $ inv current_workspace_element
          new_workspace_idx = (mult local_rotation $ current_workspace_element) !! 0
          new_workspace_label = index2label new_workspace_idx


-- Shift focused window to workspace and view the workspace
-- Assumes that you have 24 workspaces named "1", "2", ... "24"
cubic_shift :: OctahedralPermutation -> WindowSet -> WindowSet
cubic_shift = cubic_shift_custom label2index_1_to_24 index2label_1_to_24
    where label2index_1_to_24 label = scanString label - 1
          index2label_1_to_24 workspace_idx = show $ workspace_idx + 1
