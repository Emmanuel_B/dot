-- xmonad.hs Configuration file of the file manager.
--
-- TODO(Manu): Add the reference to the other configuration files that are used


--               \\XXXXXX//
--                XXXXXXXX
--               //XXXXXX\\                      OOOOOOOOOOOOOOOOOOOO
--              ////XXXX\\\\                     OOOOOOOOOOOOOOOOOOOO
--             //////XX\\\\\\     |||||||||||||||OOOOOOOOOOOOOOOVVVVVVVVVVVVV
--            ////////\\\\\\\\    |!!!|||||||||||OOOOOOOOOOOOOOOOVVVVVVVVVVV'
--           ////////  \\\\\\\\ .d88888b|||||||||OOOOOOOOOOOOOOOOOVVVVVVVVV'
--          ////////    \\\\\\\d888888888b||||||||||||            'VVVVVVV'
--          ///////      \\\\\\88888888888||||||||||||             'VVVVV'
--          //////        \\\\\Y888888888Y||||||||||||              'VVV'
--          /////          \\\\\\Y88888Y|||||||||||||| .             'V'
--          ////            \\\\\\|iii|||||||||||||||!:::.            '
--          ///              \\\\\\||||||||||||||||!:::::::.
--          //                \\\\\\\\           .:::::::::::.
--          /                  \\\\\\\\        .:::::::::::::::.
--                              \\\\\\\\     .:::::::::::::::::::.
--                               \\\\\\\\
--          
--          by Bob Allison

{-# LANGUAGE DeriveDataTypeable #-}


import Data.Monoid
import qualified Data.Map as M
import System.IO
import Graphics.X11.ExtraTypes.XF86

import XMonad
import qualified XMonad.StackSet as W
import XMonad.Actions.CycleWS
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.FadeWindows
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeys, additionalKeysP)
import qualified XMonad.Util.ExtensibleState as XState

import Octahedral


-- For info:
-- windows :: (WindowSet -> WindowSet) -> X
-- W.view :: (Eq s, Eq i) => i -> W.StackSet i l a s sd -> W.StackSet i l a s sd
-- type WindowSet = StackSet WorkspaceId (Layout Window) Window ScreenId ScreenDetail
-- W.currentTag :: W.StackSet i l a s sd -> i


main :: IO()
main = do
    xmproc <- spawnPipe "xmobar"

    xmonad $ docks $ ewmh $ def 
        { manageHook = manageDocks <+> myManageHook <+> manageHook def
        , layoutHook = avoidStruts  $  layoutHook def
        , logHook = fadingHook <+> dynamicLogWithPP xmobarPP
                        { ppOutput = hPutStrLn xmproc
                        , ppTitle = xmobarColor "green" "" . shorten 50
                        }
		, handleEventHook = handleEventHook def `mappend` handleKeyEventHook
        , modMask = mod4Mask     -- Rebind Mod to the Windows key
        , terminal = "gnome-terminal"
        , workspaces = myWorkspaces
        } `additionalKeys` my_keys `additionalKeysP` my_keys_EZ


myWorkspaces :: [WorkspaceId]
myWorkspaces = map show [1 .. 24 :: Int]


-- https://wiki.haskell.org/Xmonad/General_xmonad.hs_config_tips#ManageHook_examples
-- can use xprop to find information about a window
myManageHook :: ManageHook
myManageHook = composeAll . concat $ [ 
    [ className =? "Chromium" --> doShift "3" ],
    [ className =? "Upwork" --> doShift "9" ]
    ]


newtype MyState = MyState { seethrough :: Rational } deriving (Show, Read, Typeable)


instance ExtensionClass (MyState) where
	initialValue  = MyState 0 


fadingHook :: X ()
fadingHook = do 
		transparency_value <- XState.gets seethrough
		fadeWindowsLogHook $ composeAll [ 
						transparency transparency_value,
                        isUnfocused --> transparency 0.2
                        ]



my_keys :: [((KeyMask, KeySym), X ())]
my_keys = [ 
        ((mod4Mask .|. shiftMask, xK_z), spawn "xscreensaver-command -lock; xset dpms force off")
        , ((controlMask, xK_Print), spawn "sleep 0.2; scrot -s")
        , ((0, xK_Print), spawn "scrot")
        -- a basic CycleWS setup: mod+l-h arrows switches workspaces
        , ((mod4Mask,               xK_l),  nextWS)
        , ((mod4Mask,               xK_h),  prevWS)
        , ((mod4Mask .|. shiftMask, xK_l),  shiftToNext)
        , ((mod4Mask .|. shiftMask, xK_h),  shiftToPrev)
		-- Cubic switching of workspaces
        , ((mod4Mask,               xK_m),  windows $ cubic_switch Octahedral.maybe)
        , ((mod4Mask,               xK_y),  windows $ cubic_switch Octahedral.yes)
        , ((mod4Mask,               xK_n),  windows $ cubic_switch Octahedral.no)
        , ((mod4Mask .|. shiftMask, xK_m),  windows $ cubic_shift Octahedral.maybe)
        , ((mod4Mask .|. shiftMask, xK_y),  windows $ cubic_shift Octahedral.yes)
        , ((mod4Mask .|. shiftMask, xK_n),  windows $ cubic_shift Octahedral.no)

		-- This will allow the mod key to be captured by the custom key hook
		-- that does the transparency
        , ((noModMask,              xK_Super_L),  return ())
        
         -- resizing the master/slave ratio
        , ((mod4Mask,               xK_Down     ), sendMessage Shrink) -- %! Shrink the master area
        , ((mod4Mask,               xK_Up     ), sendMessage Expand) -- %! Expand the master area

        -- set brightness to associated media keys
        , ((0, xF86XK_MonBrightnessUp), spawn "lux -a 10%")
        , ((0, xF86XK_MonBrightnessDown), spawn "lux -s 10%")
        ]


-- keys in Emacs "EZ" format
my_keys_EZ = [
    -- mute, lower and increase sound with respective multimedia keys
    ("<XF86AudioMute>", spawn "amixer -D pulse set Master 1+ toggle")
    , ("<XF86AudioLowerVolume>", spawn "amixer -D pulse set Master 10%-")
    , ("<XF86AudioRaiseVolume>", spawn "amixer -D pulse set Master 10%+")
    ]


on_switch_transparency :: Rational
on_switch_transparency = 0.75

keyDownActions :: XConf -> M.Map (KeyMask, KeySym) (X ())
keyDownActions (XConf{ config = XConfig {XMonad.modMask = modMask} }) = M.fromList $ 
    [ 
	  -- pressing left mod key
	  ((noModMask, xK_Super_L), XState.put (MyState on_switch_transparency) >> windows id)
    ]

keyUpActions :: XConf -> M.Map (KeyMask, KeySym) (X ())
keyUpActions (XConf{ config = XConfig {XMonad.modMask = modMask} }) = M.fromList $ 
    [ 
	  -- releasing left mod key
	  ((mod4Mask, xK_Super_L), XState.put (MyState 0) >> windows id)
    ]

handleKeyEvent :: Event -> X ()
handleKeyEvent (KeyEvent {ev_event_type = eventType, ev_state = mask, ev_keycode = code})
    | eventType == keyRelease = 
		withDisplay $ \dpy -> do
        keyPressed  <- io $ keycodeToKeysym dpy code 0
        maskClean <- cleanMask mask
        keyMappings <- asks keyUpActions
        userCodeDef () $ whenJust (M.lookup (maskClean, keyPressed) keyMappings) id
    | eventType == keyPress = 
		withDisplay $ \dpy -> do
        keyPressed  <- io $ keycodeToKeysym dpy code 0
        maskClean <- cleanMask mask
        keyMappings <- asks keyDownActions
        userCodeDef () $ whenJust (M.lookup (maskClean, keyPressed) keyMappings) id
handleKeyEvent _ = return ()

handleKeyEventHook :: Event -> X All
handleKeyEventHook e = handleKeyEvent e >> return (All True)
