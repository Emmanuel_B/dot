export PATH="$HOME/.local/bin:$PATH"

alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'

set -U fish_user_paths (ruby -e 'print Gem.user_dir')/bin $fish_user_paths
